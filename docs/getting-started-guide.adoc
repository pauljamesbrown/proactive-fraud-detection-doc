:icons: font

The majority of the lab work will be completed using applications hosted on the Openshift platform: +

1. *Code Ready Workspaces* - Used for editing the Active MQ Streams Code +
2. *RHPAM Business Central* - Used to modify the Process Automation Case Management Application
3. *Openshift Console* - Used to manage the openshift plaform. +

====
INFO: Lab Openshift URL ???
====

== Code Ready Workspace Setup
====

*Step 1*: Open the code ready work space using the following URL:
    >> May need to show how to do this via an oc command <<

*Step 2*: View the AMQ Streams Code. +

*Step 3*: Review the AMQ Topic. 

*Step 4*: Build and deploy AMQ Streams Code in Code Ready Workspace. +
- 
====

== Fuse Code Setup

====
*Step 1*: Login to the Code Ready Workspaces environment . +

*Step 2*: Open the workspace environment. +
- Click on the "Workspaces" menu option on the left had side menu +
- Select the Workspace +

- Click "Create & Open"

*Step 2*: Open the workspace environment. +
- Points to note:
-- Git commits +
-- Deployment process +
-- Build process +
====